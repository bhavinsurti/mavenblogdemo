<?php

return [
	'userpic' => 'photos',
	'userpicthumb' => 'photos/thumbs/',
	'blogimg' => 'blogimg/',
];