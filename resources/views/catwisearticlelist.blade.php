@extends('default')

@section('content')
<!-- Header -->
    
    <div class="container">

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">
                {{ Session::get('message') }}
            </p>
        @endif
    
        <div class="row">
            <div class="col-md-8">

                <h1>Latest Posts of {!! $categoryname !!}</h1>
                <hr>
                {!! $articleHtml !!}
    
                <ul class="pager">
                    <li class="previous"><a href="#">&larr; Previous</a></li>
                    <li class="next"><a href="#">Next &rarr;</a></li>
                </ul>

            </div>
            
            @include('rightside')
        </div>
    </div>

@endsection