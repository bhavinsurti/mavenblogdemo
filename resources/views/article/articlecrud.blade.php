@extends('default')
        
@section('content')
<!-- Body -->
<div class="container">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    {{
        Form::open(
            array(
                'action'    => array('ArticleController@createarticle'),
                'class'     => 'form-horizontal',
                'method'    => 'post',
                'id'        => 'frmRegistration',
                'name'      => 'frmRegistration',
                'files'     => true,
            )
        )
    }}
    <fieldset>
        
        <legend>Create an Article</legend>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="fn">Title</label>
            <div class="col-md-8">
                {{ 
                    Form::text('title','',array('placeholder'=>'Article Title','class'=>'form-control input-md','id'=>'title')) 
                }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="ln">Article Content</label>
            <div class="col-md-8">
                {{ 
                    Form::textarea('content','',array('placeholder'=>'Last name','class'=>'form-control input-md','id'=>'content'))
                }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Category</label>
            <div class="col-md-8">
                {{-- */ $catarr = array('1'=>'News','2'=>'Articles','3'=>'Gaming') /* --}}
                <?php $catarr = array(''=>'Select','1'=>'News','2'=>'Articles','3'=>'Gaming') ?>
                {{ 
                    Form::select('cat_id',$catarr,null,array('class'=>'form-control input-md'))
                }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Image</label>
            <div class="col-md-8">
                {{ 
                    Form::file('arti_image','',null,array('id'=>'arti_image'))
                }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="submit"></label>
            <div class="col-md-4">
                <button id="submit" name="submit" class="btn btn-primary">SUBMIT</button>
            </div>
        </div>


    </fieldset>
    {!! Form::close(); !!}
</div>

<script type="text/javascript">

$(function() {
   tinymce.init({
        selector: '#content'
    });
});
</script>

@endsection