@extends('default')
        
@section('content')
<!-- Body -->
<div class="container">
    
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    {{
        Form::open(
            array(
                'action'    => array('UserController@registrationform'),
                'class'     => 'form-horizontal',
                'method'    => 'post',
                'id'        => 'frmRegistration',
                'name'      => 'frmRegistration',
            )
        )
    }}
    
    <fieldset>
        
        <legend>Registration</legend>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="fn">First name</label>
            <div class="col-md-4">
                {{ 
                    Form::text('firstname','',array('placeholder'=>'First name','class'=>'form-control input-md','id'=>'firstname')) 
                }}
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="ln">Last name</label>
            <div class="col-md-4">
                {{ 
                    Form::text('lastname','',array('placeholder'=>'Last name','class'=>'form-control input-md','id'=>'firstname')) 
                }}
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email</label>
            <div class="col-md-4">
                {{ 
                    Form::email('emailid','',array('placeholder'=>'Email Id','class'=>'form-control input-md','id'=>'emaiid')) 
                }}
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="add1">Address 1</label>
            <div class="col-md-4">
                {{ 
                    Form::textarea('address1','',array('placeholder'=>'Address 1','class'=>'form-control input-md','id'=>'address1','cols'=>'1','rows'=>'5')) 
                }}
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="address2">Address 2</label>
            <div class="col-md-4">
                {{
                    Form::textarea('address2','',array('placeholder'=>'Address 2','class'=>'form-control input-md','id'=>'address2','cols'=>'1','rows'=>'5'))
                }}
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="city">City</label>
            <div class="col-md-4">
                {{
                    Form::select('cityid',array('1'=>'Surat','2'=>'Vadodra','3'=>'Ahemdabad'),'',array('class'=>'form-control input-md'))
                }}
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="zip">Birt Date</label>
            <div class="col-md-4">
                <div class='input-group date' id='datetimepicker1'>
                    {{
                        Form::text('birthdate','',array('class'=>'form-control','id'=>'birthdate'))
                    }}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-4">
                {{
                    Form::text('password','',array('class'=>'form-control input-md','id'=>'password'))
                }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="cfmpassword">Confirm Password</label>
            <div class="col-md-4">
                {{
                    Form::text('cfmpassword','',array('class'=>'form-control input-md','id'=>'cfmpassword'))
                }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="phone">Mobile Number</label>
            <div class="col-md-4">
                {{ Form::number('phone','',array('class'=>'form-control input-md','id'=>'phone')) }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="gender">Gender</label>
            <div class="col-md-4">
                
                <label class="radio-inline" for="Gender-0">
                    {{ Form::radio('gender','Male',false,array('id'=>'Gender-male')) }}Male
                </label>
                <label class="radio-inline" for="Gender-1">
                    {{ Form::radio('gender','Female',false,array('id'=>'Gender-female')) }}Female
                </label>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="submit"></label>
            <div class="col-md-4">
                <button id="submit" name="submit" class="btn btn-primary">SUBMIT</button>
            </div>
        </div>
    </fieldset>
    {!! Form::close(); !!}
</div>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>

@endsection