@extends('default')
		
@section('content')
<!-- Body -->
<div class="container">
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
					<li>
						{{ $error }}
					</li>
				@endforeach
			</ul>
		</div>
	@endif
	{{
	Form::open(
	array(
	'action' 	=> array('UserController@chngpwd',$id),
	'class' 	=> 'form-horizontal',
	'method'	=> 'post',
	'id'		=> 'frmRegistration',
	'name'		=> 'frmRegistration',
	)
	)
	}}
	<fieldset>
		<legend>Change Password</legend>
		
		<div class="form-group">
			<label class="col-md-4 control-label" for="fn">New Password</label>
			<div class="col-md-4">
				{{ Form::password('password_new',array('placeholder'=>'New Password','class'=>'form-control input-md','id'=>'password_new')) }}
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-md-4 control-label" for="ln">Retype Password</label>
			<div class="col-md-4">
				{{ Form::password('password_new_conf',array('placeholder'=>'Retype New Password','class'=>'form-control input-md','id'=>'password_new_conf')) }}
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="col-md-4 control-label" for="submit"></label>
			<div class="col-md-4">
				<button id="submit" name="submit" class="btn btn-primary">SUBMIT</button>
			</div>
		</div>
	</fieldset>
	{!! Form::close(); !!}
</div>
@endsection