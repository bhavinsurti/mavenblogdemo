<!-- Header -->
<header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">           
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{ URL::to('/') }}" class="navbar-brand">Maven Blog</a>
        </div>
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <!--<form class="navbar-form navbar-right" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
            </form>-->
            <ul class="nav navbar-nav">loginhtml">Home</a></li>
                @if (!Auth::check())
                    <li><a href="{{ URL::to('login') }}">Login</a></li>
                @endif
            </ul>
            @if (Auth::check())
                <div class="dropdown navbar-right">
                    @if(isset(Auth::user()->uthumb))
                        <img class="img-circle dropdown-toggle" data-toggle="dropdown" width="50" style="margin:9px;" src="{{ URL::to(config('mavenblog.userpicthumb').Auth::user()->uthumb) }}" alt="">    
                    @endif
                    <ul class="nav navbar-nav dropdown-menu">
                    
                        <li><a href="{{ URL::to('editprofile',Auth::user()->id) }}">Edit Profile</a></li>
                        <li><a href="{{ URL::to('chngpwd',Auth::user()->id) }}">Change Password</a></li>
                        <li><a href="{{ URL::to('changeprofile') }}">Profile Pic</a></li>
                        <li><a href="{{ URL::to('create-article') }}">Create Article</a></li>
                        <li><a href="{{ URL::to('logout') }}">Logout</a></li>
                    
                    </ul>
                </div>
            @endif
        </nav>
    </div>
</header>
