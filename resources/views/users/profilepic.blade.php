@extends('default')
		
@section('content')
<!-- Body -->
<div class="container">
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
					<li>
						{{ $error }}
					</li>
				@endforeach
			</ul>
		</div>
	@endif
	{{
	Form::open(
	array(
	'action' 	=> array('UserController@changeprofile'),
	'class' 	=> 'form-horizontal',
	'method'	=> 'post',
	'id'		=> 'frmRegistration',
	'name'		=> 'frmRegistration',
	'files' 	=> true
	)
	)
	}}
	<fieldset>
		<!-- Form Name -->
		<legend>Profile Pic Upload</legend>
		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-4 control-label" for="fn">Upload Profile</label>
			<div class="col-md-4">
				{{ Form::file('userpic','',array('placeholder'=>'First name','class'=>'form-control input-md','id'=>'userpic')) }}
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-4 control-label" for="submit"></label>
			<div class="col-md-4">
				<button id="submit" name="submit" class="btn btn-primary">Upload</button>
			</div>
		</div>

	</fieldset>
	{!! Form::close(); !!}
</div>

<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker();
	});
</script>