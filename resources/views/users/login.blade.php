<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A Bootstrap Blog Template">
        <meta name="author" content="Vijaya Anand">
        <title>Home</title>
        <!-- Bootstrap CSS file -->
        <link href="{{ URL::asset('theme') }}/lib/bootstrap-3.0.3/css/bootstrap.min.css" rel="stylesheet" />
        <link href="{{ URL::asset('theme') }}/lib/bootstrap-3.0.3/css/bootstrap-theme.min.css" rel="stylesheet" />
        <link href="{{ URL::asset('theme') }}/blog.css" rel="stylesheet" />
    </head>
    <body>
        <div class="container">
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class') }}">
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="row">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Sign in</h1>
                    <div class="account-wall">
                        {{ Form::open(array('action'=>array('UserController@login'),'class'=>'form-signin','name'=>'form_login','method'=>'post')) }}
                            {{ Form::text('email_id',null,array('class'=>'form-control','autofocus'=>true,'placeholder'=>'Email Id')) }}
                            {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                            <label class="checkbox pull-left">
                                {{ Form::checkbox('remember-me',null) }}Remember me
                            </label>
                        {{ Form::close() }}
                    </div>
                    <a href="{{ URL::to('registration') }}" class="text-center new-account">Create an account </a>
                </div>
            </div>
        </div>
        @include('users.footer')


        <style type="text/css">
            .form-signin
        {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .form-signin-heading, .form-signin .checkbox
        {
            margin-bottom: 10px;
        }
        .form-signin .checkbox
        {
            font-weight: normal;
        }
        .form-signin .form-control
        {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .form-signin .form-control:focus
        {
            z-index: 2;
        }
        .form-signin input[type="text"]
        {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"]
        {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        .account-wall
        {
            margin-top: 20px;
            padding: 40px 0px 20px 0px;
            background-color: #f7f7f7;
            -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        }
        .login-title
        {
            color: #555;
            font-size: 18px;
            font-weight: 400;
            display: block;
        }
        .profile-img
        {
            width: 96px;
            height: 96px;
            margin: 0 auto 10px;
            display: block;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
        }
        .need-help
        {
            margin-top: 10px;
        }
        .new-account
        {
            display: block;
            margin-top: 10px;
        }
        </style>
    </body>
</html>