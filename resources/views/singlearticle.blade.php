@extends('default')

@section('content')
    <div class="container">

        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">
                {{ Session::get('message') }}
            </p>
        @endif
    
        <div class="row">

            <div class="col-md-8">
                {!! $articleHtml !!}           

                <ul class="pager">
                    <li class="previous"><a href="index.html">&larr; Back to posts</a></li>
                </ul>   

                <!-- Comment form -->
                <div class="well">
                    <h4>Leave a comment</h4>
                    <form role="form" class="clearfix">
                      <div class="col-md-6 form-group">
                        <label class="sr-only" for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name">
                      </div>
                      <div class="col-md-6 form-group">
                        <label class="sr-only" for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="Email">
                      </div>
                      <div class="col-md-12 form-group">
                        <label class="sr-only" for="email">Comment</label>
                        <textarea class="form-control" id="comment" placeholder="Comment"></textarea>
                      </div>
                      <div class="col-md-12 form-group text-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </form>                 
                </div>

                <hr />

                <ul id="comments" class="comments">
                    <li class="comment">
                        <div class="clearfix">
                            <h4 class="pull-left">John</h4>
                            <p class="pull-right">9:41 PM on August 24, 2013</p>
                        </div>
                        <p>
                            <em>I don't believe in astrology but still your writing style is really great!</em>
                        </p>
                    </li>

                    <li class="comment clearfix">
                        <div class="clearfix">
                            <h4 class="pull-left">John</h4>
                            <p class="pull-right">9:41 PM on August 24, 2013</p>
                        </div>
                        <p>
                            <em>I don't believe in astrology but still your writing style is really great!</em>
                        </p>
                    </li>
                </ul>
            </div>
            @include('rightside')
        </div>
    </div>
@endsection