<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A Bootstrap Blog Template">
        <meta name="author" content="Vijaya Anand">
        <title>Home</title>
        <!-- Bootstrap CSS file -->
        <link href="{{ URL::asset('theme') }}/lib/bootstrap-3.0.3/css/bootstrap.min.css" rel="stylesheet" />
        <link href="{{ URL::asset('theme') }}/lib/bootstrap-3.0.3/css/bootstrap-theme.min.css" rel="stylesheet" />
        <link href="{{ URL::asset('theme') }}/blog.css" rel="stylesheet" />

        
        <script src="{{ URL::asset('theme') }}/lib/jquery-2.0.3.min.js"></script>
        <script src="{{ URL::asset('theme') }}/lib/bootstrap-3.0.3/js/bootstrap.min.js"></script>
        <script src="{{ URL::asset('theme') }}/lib/bootstrap-3.0.3/js/moment.js"></script>
        <script src="{{ URL::asset('theme') }}/lib/bootstrap-3.0.3/js/bootstrap-datetimepicker.js"></script>
        <script src="{{ URL::asset('tinymce') }}/tinymce.min.js"></script>

</head>

<body>

	@include('users.headernav')

	@yield('content')

	@include('users.footer')
	
</body>
</html>