<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/registration','UserController@registrationform');
Route::post('/registration','UserController@registrationform');

Route::any('/login','UserController@login');

Route::any('/','PagesController@index');

Route::any('/editprofile/{id}','UserController@editprofile')->middleware('author');;

Route::get('/logout','UserController@logout');

Route::any('/chngpwd/{id}','UserController@chngpwd')->middleware('author');;

Route::any('/changeprofile','UserController@changeprofile')->middleware('author');;

Route::any('/create-article','ArticleController@createarticle')->middleware('author');

Route::get('/cat/{catname}/{catid}','PagesController@showArticleByCategory');

Route::get('/{catname}/{articleid}/{articleslug}','PagesController@showSingleArticle');