<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use URL;

class LoadArticleHtml extends ServiceProvider
{

    protected $htmlArticle = '';

    public function __construct($app)
    {
        parent::__construct($app);

    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Providers\LoadArticleHtml',function($app){
            return new LoadArticleHtml($app);
        });
    }

    public function createHtml($allArticle = array()){

        $this->htmlArticle = '';

        if(!empty($allArticle)){

            foreach($allArticle as $article) {

                $this->htmlArticle .= '
                                        <article>
                                            <h2>
                                                <a href="'  . URL::to(
                                                                    $article->category->categoryname
                                                                    . '/'
                                                                    . $article->id
                                                                    . '/'
                                                                    . create_slug($article->title)
                                                                )
                                                            . '">'
                                                            . $article->title
                                                            . '</a>
                                            </h2>
                                            <div class="row">
                                                <div class="col-sm-6 col-md-6">
                                                    <span class="glyphicon glyphicon-folder-open"></span> &nbsp;
                                                            <a href="' . URL::to(
                                                                                    'cat/'
                                                                                    .$article->category->categoryname
                                                                                    .'/'
                                                                                    .$article->category->id
                                                                                ) 
                                                                        . '">'
                                                                        . $article->category->categoryname
                                                                        . '</a>
                                                    &nbsp;&nbsp;
                                                    <span class="glyphicon glyphicon-bookmark"></span> 
                                                    <a href="">Aries</a>, 
                                                    <a href="#">Fire</a>, 
                                                    <a href="#">Mars</a>
                                                </div>
                                                <div class="col-sm-6 col-md-6">
                                                    <span class="glyphicon glyphicon-pencil"></span> 
                                                    <a href="singlepost.html#comments">20 Comments</a>&nbsp;&nbsp;
                                                    <span class="glyphicon glyphicon-time"></span>
                                                        '. \Carbon\Carbon::parse($article->created_at)->format('F j, Y g:i a') .'
                                                </div>
                                            </div>

                                            <hr>';

                if(!empty($article->arti_image)) {
                    $this->htmlArticle .= '<img src="'.URL::to(config('mavenblog.blogimg'))
                                                      .'/'
                                                      .$article->arti_image.' " 
                                                      class="img-responsive">';
                }
                                            

                $this->htmlArticle .= '<br />
                                        <p class="lead">'. $article->fulldesc. '</p>
                                            <p class="text-right">
                                                <a href="'  . URL::to(
                                                                    $article->category->categoryname
                                                                    . '/'
                                                                    . $article->id
                                                                    . '/'
                                                                    . create_slug($article->title)
                                                                )
                                                            . '">'
                                                            . $article->title
                                                            . '</a>
                                            </p>

                                              <hr>
                                        </article>';

                
            }
        } else {
            $this->htmlArticle = 'No Data Found';
        }

        return $this->htmlArticle;
        
    }

    public function createHtmlCatWise($allArticleByCat = array()){

        $this->htmlArticle = '';

        if(!empty($allArticleByCat)){

            foreach($allArticleByCat as $ArticleByCat) {
                foreach($ArticleByCat->article as $article) {
                   
                    $this->htmlArticle .= '
                                            <article>
                                                <h2>
                                                    <a href="'  . URL::to(
                                                                        $ArticleByCat->categoryname
                                                                        . '/'
                                                                        . $article->id
                                                                        . '/'
                                                                        . create_slug($article->title)
                                                                    )
                                                                . '">'
                                                                . $article->title
                                                                . '</a>
                                                </h2>
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-6">
                                                        <span class="glyphicon glyphicon-folder-open"></span> &nbsp;
                                                                <a href="' . URL::to(
                                                                                        'cat/'
                                                                                        .$ArticleByCat->categoryname
                                                                                        .'/'
                                                                                        .$ArticleByCat->id
                                                                                    ) 
                                                                            . '">'
                                                                            . $ArticleByCat->categoryname
                                                                            . '</a>
                                                        &nbsp;&nbsp;
                                                        <span class="glyphicon glyphicon-bookmark"></span> 
                                                        <a href="">Aries</a>, 
                                                        <a href="#">Fire</a>, 
                                                        <a href="#">Mars</a>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6">
                                                        <span class="glyphicon glyphicon-pencil"></span> 
                                                        <a href="singlepost.html#comments">20 Comments</a>&nbsp;&nbsp;
                                                        <span class="glyphicon glyphicon-time"></span>
                                                            '. \Carbon\Carbon::parse($article->created_at)->format('F j, Y g:i a') .'
                                                    </div>
                                                </div>

                                                <hr>';

                    if(!empty($article->arti_image)) {
                        $this->htmlArticle .= '<img src="'.URL::to(config('mavenblog.blogimg'))
                                                          .'/'
                                                          .$article->arti_image.' " 
                                                          class="img-responsive">';
                    }
                                                

                    $this->htmlArticle .= '<br />
                                            <p class="lead">'. $article->fulldesc. '</p>
                                                <p class="text-right">
                                                    <a href="'  . URL::to(
                                                                        $ArticleByCat->categoryname
                                                                        . '/'
                                                                        . $article->id
                                                                        . '/'
                                                                        . create_slug($article->title)
                                                                    )
                                                                . '">'
                                                                . $article->title
                                                                . '</a>
                                                </p>

                                                  <hr>
                                            </article>';

                    
                }
            }
        } else {
            $this->htmlArticle = 'No Data Found';
        }

        return $this->htmlArticle;
        
    }
}
