<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'category';

    protected $fillable = ['id', 'categoryname'];
    public function article(){
    	return $this->hasMany('App\Article','cat_id','id');
    }
}
