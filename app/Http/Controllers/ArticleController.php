<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Article;
use Image;
use File;

class ArticleController extends Controller
{
    //

    public function __construct(){
        if(!Auth::check()) {
            
            $msg = array(
                'message'=>'For creating Article,you mush have to login!',
                'alert-class'=>'alert-warning'
            );
            return redirect()->action('UserController@login',$msg);
        }
    }

    public function createarticle(Request $req){

        if($req->isMethod('post')){
        
            $this->validate(
                $req,
                [
                    'title' => 'required',
                    'content' => 'required',
                    'cat_id' => 'required',
                    'arti_image' => 'mimes:jpeg,png,jpg',
                ],
                [
                    'title.required' => 'Please insert Title',
                    'content.required' => 'Plese insert Article Content',
                    'cat_id.required' => 'Please select any Category',
                    'cat_id.arti_image' => 'Please upload jpg,jpeg or png image',
                ]
            );


            $allData = array();
            $allData['title'] = $req->input('title');
            $allData['fulldesc'] = $req->input('content');
            $allData['cat_id'] = $req->input('cat_id');
            $allData['u_id'] = Auth::user()->id;

            if($req->hasFile('arti_image')) {
                $path = public_path(config('mavenblog.blogimg'));
                if(!File::exists($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }
                
                $image = Image::make($req->file('arti_image')->getRealPath());
                $originalFilename =  uniqid() .'.jpg';
                $allData['arti_image'] = $originalFilename; 
                $image->resize(750, null)->save($path.$originalFilename);
            }

            Article::create($allData);

        }

        return view('article.articlecrud');
    }

}
