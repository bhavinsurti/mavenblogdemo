<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Article;
Use Carbon\Carbon;
use App\Category;
use DB;
use App\Providers\LoadArticleHtml;

class PagesController extends Controller
{
    //
    public function index(LoadArticleHtml $loadArticle){

        $data['allArticle'] = Article::with('category')->orderBy('created_at', 'desc')->get();
        $datapassed['articleHtml'] = $loadArticle->createHtml($data['allArticle']);
        return view('index',$datapassed);
    }

    public function showArticleByCategory(LoadArticleHtml $loadArticle,$catname,$catId){

        //DB::enableQueryLog();

        $data['allArticle'] = Category::where('category.id',$catId)->
                                with([
                                        'article' => function($query) use ($catId){
                                                        $query->orderBy('created_at', 'desc');
                                                    }
                                    ])->get();
        $datapassed['articleHtml'] = $loadArticle->createHtmlCatWise($data['allArticle']);
        $datapassed['categoryname'] = $data['allArticle'][0]->categoryname;

        /*$data['allArticle'] = Category::where('category.id',$catId)->
                                with(
                                    ['article' => function($query) use ($catId,$articleId){
                                                        $query->where('cat_id', $catId);
                                                        $query->where('id', $articleId);
                                                        $query->orderBy('created_at', 'desc');
                                                    }
                                    ])->get()->toArray();
        */
        /*echo "<pre>";
            print_r($data['allArticle']);
            dd(DB::getQueryLog());
        echo "</pre>";
        exit(0);*/


        return view('catwisearticlelist',$datapassed);
    }

    public function showSingleArticle(LoadArticleHtml $loadArticle,$catname,$articleId,$articleslug){
        $data['article'] = Article::where('id',$articleId)->with('category')->get();
        $datapassed['articleHtml'] = $loadArticle->createHtml($data['article']);

        return view('singlearticle',$datapassed);
    }
}
