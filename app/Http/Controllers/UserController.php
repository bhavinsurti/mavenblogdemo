<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
Use App\User;
Use Carbon\Carbon;
use Auth;
use Image; 

class UserController extends Controller
{
 
 	public function __construct(){
 		if (!Auth::check()) {
    		//echo 'please do login';
    	}	
 	}   

    public function registrationform(Request $request){

    	
    	if ($request->isMethod('post'))
		{
		   	$input = $request->all();
   	
   			$this->validate($request,[
	   			'firstname' => 'required',
	   			'lastname' => 'required',
	   			'emailid' => 'required|email|unique:users,email',
	   			'address1' => 'required',
	   			'cityid' => 'required',
	   			'birthdate' => 'required',
	   			'password' => 'required|min:5',
	   			'cfmpassword' => 'same:password',
	   			'phone' => 'required',
	   			'gender' => 'required',
	   		]);

	   		$password = $request->input('password');
	   		$birthdate = $request->input('birthdate');

	   		$insertData = array(
	   			'firstname' => $request->input('firstname'),
	   			'lastname' => $request->input('lastname'),
	   			'email' => $request->input('emailid'),
	   			'address1' => $request->input('address1'),
	   			'address2' => $request->input('address2'),
	   			'city_id' => $request->input('cityid'),
	   			'birthdate' => Carbon::parse($birthdate)->format('Y-m-d'),
	   			'password' => Hash::make($password),
	   			'mobilenumber' => $request->input('phone'),
	   			'gender' => $request->input('gender'),
	   		);

	   		User::create($insertData);

	   		$msg = array(
	   			'message'=>'Your are registered successfully, Please login too continue!',
	   			'alert-class'=>'alert-success'
	   		);
	   		return redirect()->action('UserController@login',$msg);

		} else {
			return view('users.registration');	
		}

    }

    public function login(Request $req)
    {
    	if (Auth::check()) {
    		return redirect()->action('PagesController@index');
    	} else {

	    	$datapassed = $req->all();

	    	if ($req ->isMethod('post'))
			{
				$email = $req->input('email_id');
				$password = $req->input('password');
				$remember = $req->input('remember-me');
				if (Auth::attempt(['email' => $email, 'password' => $password],$remember)) {
					return redirect()->action('PagesController@index');
				} 
				
			} 

		} 

		return view('users.login');
    }

    public function editprofile($id,Request $req)
    {
    	if (Auth::check()) {
    		
    		if ($req ->isMethod('post'))
			{
				$user = User::findorFail($id);

				$this->validate($req,[
		   			'firstname' => 'required',
		   			'lastname' => 'required',
		   			'address1' => 'required',
		   			'cityid' => 'required',
		   			'birthdate' => 'required',
		   			'phone' => 'required',
		   			'gender' => 'required',
		   		]);

		   		$datatoUpdate = $req->all();

		   		$user->update($datatoUpdate);

		   		$msg = array(
	   						'message'=>'Your profile has been updated successfully!',
	   						'alert-class'=>'alert-success'
	   					);
		   		return redirect()->action('PagesController@index')->with($msg); 
			} 
    		
    		$data = User::where('id',$id)->get()->first();
    		$data['userId'] = Auth::id();
    		return view('users.editprofile',$data);

    	} else {
    		return redirect()->action('UserController@login');	
    	}
    }

    public function logout() {

    	Auth::logout();
    	return redirect()->action('PagesController@index');
    }

    public function chngpwd($id,Request $req){

    	if (Auth::check()) {
    		
    		if ($req ->isMethod('post'))
			{
				$user = User::findorFail($id);

				$this->validate($req,[
		   			'password_new' => 'required|min:5',
	   				'password_new_conf' => 'required|same:password_new',
		   		]);

		   		$datatoUpdate['password'] = $req->input('password_new');
		   		$datatoUpdate['password'] = Hash::make($datatoUpdate['password']);

		   		$user->update($datatoUpdate);

		   		$msg = array(
	   						'message'=>'Your password has been updated successfully, Please login in!',
	   						'alert-class'=>'alert-success'
	   					);
		   		Auth::logout();
		   		return redirect()->action('UserController@login')->with($msg); 
			}
    		
    		$data = User::where('id',$id)->get()->first();
    		$data['userId'] = Auth::id();
    		return view('users.changepassword',$data);

    	} else {
    		return redirect()->action('UserController@login');	
    	}	
    }

    public function changeprofile(request $req){
    	$id = Auth::id();
    	if (Auth::check()) {
    		
    		if($req->hasFile('userpic')) {
				$this->validate($req,[
			   			'userpic' => 'required|image|mimes:jpeg,png,jpg|max:1024',
		   			]);

				$image = Image::make($req->file('userpic')->getRealPath());
				$originalFilename =  uniqid() .'.jpg';
				$originalImagepath = public_path(config('mavenblog.userpic') . $originalFilename);
				$thumbpath = public_path(config('mavenblog.userpicthumb') . $originalFilename);
				
				$image->fit(300, 200)->save($thumbpath);

				User::where('id',$id)->update(array('uthumb' => $originalFilename));

				$msg = array(
	   						'message'=>'Your profile pic is updated!',
	   						'alert-class'=>'alert-success'
	   					);

				return redirect()->action('PagesController@index')->with($msg);
			}
		}
		$data['userId'] = $id;
    	return view('users.profilepic',$data);
    }
}
