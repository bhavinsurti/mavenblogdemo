<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table = 'articles';

    protected $fillable = ['title', 'fulldesc', 'cat_id', 'arti_image','u_id'];

    public function category(){
        return $this->belongsTo('App\Category','cat_id','id');
    }
}
